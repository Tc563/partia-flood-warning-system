# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 03:12:12 2020

@author: yitak
"""
from floodsystem.stationdata import build_station_list
from haversine.haversine import haversine
from floodsystem.geo import rivers_by_station_number

def run():
    stations = build_station_list()
    print("River, Number of stations:")
    print (rivers_by_station_number(stations, 9))

if __name__ == "__main__":
    print(run())