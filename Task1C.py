#Lab group - 117
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_distance
def run():
    #Test longitude and latitude
    location = (52.2053, 0.1218)
    # Build list of stations
    stations = build_station_list()
    print("Stations within 10km of city centre")
    print(stations_within_distance(stations,location,10))

if __name__ == "__main__":
    run()