from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.MonitoringStation import relative_water_level
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level

def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    stationlist = stations_highest_rel_level(stations, 10)
    for i in range(len(stationlist)):
        print(stationlist[i][0], " ", stationlist[i][1])

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
