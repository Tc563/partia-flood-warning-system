#Lab group -117

def typical_range_consistent(self):
    #Checking if data exists
    if self.typical_range is None:
        #If not data available return False
        return False
    elif self.typical_range[0] <= self.typical_range[1]:
        #If range correct return True
        return True
    else:
        #Else return false
        return False

def inconsistent_typical_range_stations(stations):
    #List to store all stations with inconsistent data
    Inconsistent = []
    #Tuple to store station names in alphabetical order
    InconsistentOrder =  ()
    for station in(stations):
        #Checking to see if data consistent
        if (typical_range_consistent(station) == False):
            #Appending if not consistent
            Inconsistent.append(station.name)
    #Ordering alphabetically
    InconsistentOrder = sorted(Inconsistent)
    return InconsistentOrder

def relative_water_level(self):
    if self.latest_level is None or typical_range_consistent(self) == False:
        return False
    else:
        fraction = (self.latest_level - self.typical_range[0]) / (self.typical_range[1] - self.typical_range[0])
        return fraction
