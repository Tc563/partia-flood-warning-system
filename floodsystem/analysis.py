# Lab group 117

import dateutil.parser
import datetime
import floodsystem.flood
import floodsystem.geo
import numpy as np
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.utils import sorted_by_key
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.flood import stations_level_over_threshold
import matplotlib
import matplotlib.dates
import scipy as sp
import scipy.misc


def issue_warnings(stations, p=4, dt=10):
    # Define (low, moderate, high, severe) risk values
    def risk_definition(risk):
        """Helper Function to define what different risks mean"""
        boundaries = (0, 0.8, 1.5, 2)
        if risk is None:
            return "unknown"
        if risk < boundaries[1]:
            return "low"
        if risk < boundaries[2]:
            return "moderate"
        if risk < boundaries[3]:
            return "high"
        return "severe"

    # Defines how much derivatives of the level matter
    dweight, d2weight = (5, 0.1)

    stations_by_risk = []
    # store maximum risk of each river for benefit of inconsistent stations
    risk_of_rivers = {}
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    # Get all the unsafe stations
    unsafe_stations_name = stations_level_over_threshold(
        stations, 0.8)  # Tol is for moderate risk stations or higher
    unsafe_stations = [s for s in stations for name,
                       level in unsafe_stations_name if s.name == name]

    for s in stations:
        if s in inconsistent_stations:
            continue
        if not s.latest_level_consistent():
            inconsistent_stations.append(s)
            continue
        if s not in unsafe_stations:
            stations_by_risk.append(
                (s, s.relative_water_level(), risk_definition(s.relative_water_level())))
            continue  # save time computing fairly safe stations

        dates, levels = fetch_measure_levels(
            s.measure_id, dt=datetime.timedelta(days=dt))
        times = matplotlib.dates.date2num(dates)
        # So simple operations can be done on the list
        try:
            levels = np.array(levels)
            levels = (levels - s.typical_range[0]) / (
                s.typical_range[1] - s.typical_range[0])
        except (TypeError, ValueError):  # For the random list in relative levels that appears
            inconsistent_stations.append(s)
            continue

        # Converts the levels to relative levels before polynomial fitting
        # since the values are more useful as relative levels.

        # f is a function representing water levels over time, and its
        # derivatives are computed. The rest are other useful values
        try:
            f, offset = polyfit(dates, levels, p)
            latest_time = times[-1] - offset
        # in case of weird empty arrays, this wont happen if new fixed code is
        # added for the datafetcher functions.
        except (IndexError, ValueError, TypeError):
            inconsistent_stations.append(s)
            continue
        df = f.deriv()
        d2f = f.deriv(2)
        risk_value = f(latest_time)
        risk_value += df(latest_time) * dweight
        risk_value += d2f(latest_time) * d2weight
        if risk_value < s.relative_water_level():
            # This just prevents strange results, like a safety factor
            risk_value = s.relative_water_level()
        if risk_value is None:  # Some weird stuff is happening with risk_value
            inconsistent_stations.append(s)
            continue

        stations_by_risk.append(
            (s, risk_value, risk_definition(risk_value)))

        if (not s.river in risk_of_rivers.keys()) or (risk_value > risk_of_rivers[s.river]):
            risk_of_rivers[s.river] = risk_value
        else:
            stations_by_risk.append((s, 0, risk_definition(0)))

    return sorted_by_key(stations_by_risk, 1, reverse=True)


def polyfit(dates, levels, p):
    times = matplotlib.dates.date2num(dates)
    d0 = np.min(times)
    times = times - d0
    poly = np.poly1d(np.polyfit(times, levels, p))

    return poly, d0
