#Lab group - 117
#importing modules
from haversine.haversine import haversine

def stations_by_distance(stations,p):
    #Function sorts stations by distance away from p
    #Requires list of stations and tuple of coordinates for inputs
    #Returns tuple of stations sorted by distance away
    
    assert type(p) == tuple
    unsortedlist = []
    sortedlist = ()
    #Creating list with name, distance
    for currentStation in(stations):
        unsortedlist.append((currentStation.name,haversine(currentStation.coord,p)))
    #Sorting list by distance away
    sortedlist = sorted(unsortedlist, key=getKey1)
    return sortedlist

def stations_within_distance(stations,p,radius):
    #Returns all of the stations within a certain radius

    #Creating list of stations sorted by distance
    StationsDistance = stations_by_distance(stations,p)
    #Keeping the names of stations within a certain radius
    i = 0
    StationsWithinRadius = []
    while (StationsDistance[i][1] <= radius):
        StationsWithinRadius.append(StationsDistance[i][0])
        i +=1
    #Sorting list into alphabetical order
    StationsAlphabetical = ()
    StationsAlphabetical = sorted(StationsWithinRadius)
    return StationsAlphabetical

#Tells sorter to sort by 2nd column in list
def getKey1(item):
    return item[1]

def rivers_with_station(stations):
    
    #make a null list
    river_list = []
    #iterating
    for i in stations:
        #find rivers with at least one station
        if i.river:
            river_list.append(i.river)
    #Avoid duplication
    sorted_list = list(dict.fromkeys(river_list))
    #return a list sorted in alphabetical order
    return sorted(sorted_list)

def stations_by_river(stations):
    
   #create an empty list
    stations_on_river = []
    
    #Create 2D Array with station names and river
    for i in stations:
        stations_on_river.append((i.name,i.river))
    
    #Create empty dict
    stations_dict = {}
    
    for i in range(len(stations_on_river)):
        if stations_on_river[i][1] in stations_dict:
            stations_dict[stations_on_river[i][1]].append(stations_on_river[i][0])
        else:
            stations_dict[stations_on_river[i][1]] = [stations_on_river[i][0]]
    return stations_dict

def bubble_sort_by_num_stations(stations):
    list_sorted = False
    while list_sorted is False:
        list_sorted = True
        for i in range(len(stations) - 1):
            i_tuple = stations[i]
            next_tuple = stations[i + 1]
            if i_tuple[1] < next_tuple[1]:
                list_sorted = False
                stations[i] = next_tuple
                stations[i + 1] = i_tuple

def rivers_by_station_number(stations, N):
    the_list = []
    dictionary = stations_by_river(stations)
    for river_key in dictionary:
        river_stations = dictionary[river_key]
        river_tuple = (river_key, len(river_stations))
        the_list.append(river_tuple)
        
    bubble_sort_by_num_stations(the_list)
    
    min_num = the_list[N - 1][1]
    cut_list = []
    for i in range(len(the_list)):
        i_tuple = the_list[i]
        i_num_stations = i_tuple[1]
        if i_num_stations >= min_num:
            cut_list.append(i_tuple)
        
    return cut_list
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    