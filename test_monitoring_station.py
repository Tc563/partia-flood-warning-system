from floodsystem.MonitoringStation import*
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.MonitoringStation import relative_water_level

def test_typical_range_consistent():
    #Assert typical_range_consistent returns a boolean value for all stations
    stations = build_station_list()
    for station in(stations):
        assert type(typical_range_consistent(station)) == bool

def test_inconsistent_typical_range_stations():
    #Asserting returns value
    stations = build_station_list()
    assert inconsistent_typical_range_stations(stations)

    #Dict with station and true false values
    stationdict = {}
    for station in(stations):
        stationdict[station.name] = typical_range_consistent(station)

    #Using stationinconsistent function to generate list of names of stations with inconsistent data
    stationinconsistent = inconsistent_typical_range_stations(stations)

    #Asserting all stations in the tuple have inconsistent data
    for i in range(len(stationinconsistent)):
        assert stationdict[stationinconsistent[i]] == False


