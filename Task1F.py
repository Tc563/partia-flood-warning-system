#Lab group - 117
from floodsystem.stationdata import build_station_list
from floodsystem.MonitoringStation import inconsistent_typical_range_stations


def run():

    #Creates list of stations
    stations = build_station_list()
    #Prints all stations with inconsistent data
    print("Stations with inconsistent data: ")
    print(inconsistent_typical_range_stations(stations))

if __name__ == "__main__":
    run()