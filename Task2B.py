from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.MonitoringStation import relative_water_level
from floodsystem.flood import stations_level_over_threshold

def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    Sorted_threshold_list = stations_level_over_threshold(stations, 0.8)
    for i in range(len(Sorted_threshold_list)):
        print(Sorted_threshold_list[i][0].name , " " , Sorted_threshold_list[i][1])

       

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
