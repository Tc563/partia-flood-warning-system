from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_distance
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def test_stations_by_distance():
    location = (52.2053, 0.1218)
    # Build list of station
    stations = build_station_list()
    #Running geo, returns tuple sorted by distance
    stationssorted = stations_by_distance(stations, location)

    #Check that both of the list and sorted tuple have same length
    assert len(stations) == len(stationssorted)

    #Runs through the list and checks that each entry is further away than previous entry
    for i in range(len(stationssorted) - 1):
        assert stationssorted[i][1] <= stationssorted[i+1][1]

def test_stations_within_range():
    location = (52.2053, 0.1218)
    # Build list of station
    stations = build_station_list()
    #Running geo, returns tuple sorted by distance
    stationssorted = stations_by_distance(stations, location)
    #Creating dictionary of stations with distance away from location
    stations_dict = {}
    for i in range(len(stationssorted)):
        stations_dict[stationssorted[i][0]] = stationssorted[i][1]

    #Using geo.py to get a list of stations within 10km
    stationsdistance = stations_within_distance(stations,location,10)

    #Assert each station in list is less than 10km away
    for i in(stationsdistance):
        assert stations_dict[i] <= 10
        
def test_rivers_with_stations():
    stations = build_station_list()
    riverlist = rivers_with_station(stations)
    #Check no duplicates
    assert len(riverlist) == len(set(riverlist))

def test_stations_by_river():
    stations = build_station_list()
    stationlist = stations_by_river(stations)
    #Check that there aren't more rivers than stations
    assert len(stationlist) <= len(stations)
    
def test_rivers_by_station_number():
    stations = build_station_list()
    testRivers = rivers_by_station_number(stations, 50)
    for i in range(49):
        if testRivers[i][1] < testRivers[i + 1][1]:
            assert False
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    