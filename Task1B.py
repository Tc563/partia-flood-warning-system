#Lab group - 117
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance
def run():
    #Test longitude and latitude
    location = (52.2053, 0.1218)
    # Build list of stations
    stations = build_station_list()
    #Running geo, returns tuple sorted by distance
    stationssorted = stations_by_distance(stations, location)
    print("10 closest stations: ")
    print(stationssorted[:10])
    print("10 furthest away stations: ")
    print(stationssorted[-10:])

if __name__ == "__main__":
    run()