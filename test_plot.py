from datetime import datetime, timedelta
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.flood import stations_highest_rel_level
import matplotlib.pyplot as plt
from floodsystem.station import MonitoringStation

def test_plot():
    DT = 1
    p = 4
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    dates = [datetime(2020, 2, 11),datetime(2020, 2, 12),datetime(2020, 2, 13),datetime(2020, 2, 14), datetime(2020, 2, 15), datetime(2020, 2, 16),
     datetime(2020, 2, 17), datetime(2020, 2, 18), datetime(2020, 2, 19),
     datetime(2020, 2, 20)]
    levels = [0.819,0.850,0.700,0.650,0.45,0.50,0.55,0.70,0.9,1.5]
    #dates = [1,2,3,4,5,6,7,8,9,10]

    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    best_fit = plot_water_level_with_fit(s, dates, levels, p)
    result =  [0.83563636, 0.80879953, 0.72543706, 0.61949301, 0.52540793, 0.47811888, 0.51305944, 0.66615967, 0.97384615, 1.47304196]
    for i in range(len(best_fit)):
        print(best_fit[i], "  ", result[i])
        assert round(best_fit[i],2) == round(result[i],2)


