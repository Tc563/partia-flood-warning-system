from floodsystem.MonitoringStation import relative_water_level
from floodsystem.station import*
from floodsystem.stationdata import*
from floodsystem.flood import*
from floodsystem.analysis import polyfit
import datetime
from floodsystem.datafetcher import fetch_measure_levels
import matplotlib
import numpy as np

def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    #Lists for stations with risk level
    lowrisk = []
    moderaterisk = []
    highrisk = []
    severerisk = []

    for station in stations:
        if relative_water_level(station) != False:
            #Stations ranked as low risk if relative level is less than or equal to twice normal range
            if relative_water_level(station) <= 2:
                lowrisk.append((station.name, station.coord))
            #Stations where the relative waterlevel is more than 3 automatically marked as severe
            elif relative_water_level(station) > 4:
                severerisk.append((station.name, station.coord))
            #Stations marked as moderate if current level outside of normal range but less than 3 and rate of change of water level is less than or euqal to 0
            elif relative_water_level(station) > 2:
                if gradient(station) >= 0:
                    # If d^2ydx^2 > 0 Mark as severe
                    if rateofchangegradient(station) >= 0:
                        severerisk.append((station.name, station.coord))
                    else:
                        highrisk.append((station.name, station.coord))
                else:
                    moderaterisk.append((station.name, station.coord))
    print("Towns with a severe flood risk")
    print(severerisk)

def gradient(station):
    DT = 2
    p = 4

    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=DT))
    if len(dates) == 0 or len(levels) == 0:
        return False
    best_fit, offset = polyfit(dates, levels, p)
    x = matplotlib.dates.date2num(dates)
    y = best_fit(x - offset)
    #List of containing gradient at each point 
    grad = np.gradient(x,y)
    #Returns average value of gradient over last 10 days
    return np.average(grad)

def rateofchangegradient(station):
    DT = 2
    p = 4

    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=DT))
    if len(dates) == 0 or len(levels) == 0:
        return False
    best_fit, offset = polyfit(dates, levels, p)
    x = matplotlib.dates.date2num(dates)
    y = best_fit(x - offset)
    #List of containing gradient at each point 
    grad = np.gradient(x,y)
    #Gradient of gradient
    rateofchange = np.gradient(x,grad)
    return np.average(rateofchange)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()