# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 00:30:09 2020

@author: yitak
"""

#lab group 117
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    rivers_number = rivers_with_station(stations)
    print("The number of rivers with at least one monitoring station is ", len(rivers_number))
    print("The first 10 rivers with at least one monitoring station are: ")
    print(rivers_number[:10])

def run2():
    stations = build_station_list()
    stations_dict = {}
    stations_dict = stations_by_river(stations)
    
    print("River Aire")
    print(sorted(stations_dict["River Aire"]))
    print("River Cam")
    print(sorted(stations_dict["River Cam"]))
    print("River Thames")
    print(sorted(stations_dict["River Thames"]))
    

if __name__ == "__main__":
    print(run())
    print(run2())