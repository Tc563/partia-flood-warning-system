from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.MonitoringStation import relative_water_level
from floodsystem.flood import*


def test_station_level_over_threshold():
    stations = build_station_list()

    update_water_levels(stations)

    Sorted_threshold_list = stations_level_over_threshold(stations, 0.8)

    for i in range(len(Sorted_threshold_list) - 1):
        assert Sorted_threshold_list[i][1] >= 0.8
        assert Sorted_threshold_list[i + 1][1] >= Sorted_threshold_list[i][1]
def test_stations_highest_rel_level():
    stations = build_station_list()

    update_water_levels(stations)

    listStation = stations_highest_rel_level(stations, len(stations) - 1)
    assert len(listStation) == len(stations) - 1